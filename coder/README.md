# Proto
make prototypes

## coder
* take an input and write an output file
* for different challenges


### com.coder.util
* general helper Classes and methods


#### HelpPrint
* com.coder.util.HelpPrint
    * static <T> String arrayToString(T[] inputArray, String splitString)
    * static String arrayToString(int[] inputArray, String splitString)
        * return String from an array (helps to print)
        * String splitString to split between elements in the array
    * static <T> String arrayToString(T[] inputArray)
    * static String arrayToString(int[] inputArray)
        * use the method above, but set default splitString as " "


#### ReadInput
* com.coder.util.ReadInput
    * static List<String> readDir(String dirName)
    * static List<String> readDir(String dirName, String extension)
        * returns a List of filepaths (as String)
        * in the given dirName with matching extension
    * static String readLine(String filename)
        * read the file from filename, then
        * return the first line
        * return null for exceptions or empty file
    * static String[] readLineToArray(String filename, String splitString) 
        * read the file from filename, then
        * returns an array of Strings
        * by splitting the first line by given splitString
    * static int[] readLineToIntArray(String filename);
        * read the file from filename, then
        * returns an array of int
        * by splitting the first line by " "
    * static int[] toIntArray(String[] strArray)
        * returns int array from strArray
        * returns null in the case of exception (NumberFormatException)


### com.coder.addictive.util
* HandleInput class
* Path class // to do
    * int color, int length, int starting, String[] movement
* Coord class
    * staic class CoordBuilder(int boardRows, int boardCols)
        * Coord build(int position)
        ```java
        Coord.CoordBuilder cb = new Coord.CoordBuilder(6, 4);
        Coord coord = cb.build(11);
        Coord c2 = Coord.CoordBuilder(6,4).build(11);
        ```
    * static int manhattanDistance(Coord c1, Coord c2)
    * Override: equals, hashcode 
* CoordPair class
    * boolean add(Coord)
    * int getDistance()
    * boolean contains(Coord c)
* Board class
    * static class BoardBuilder(String filename) // only for level1 and level2
    * static class BoardBuilder(int[] points) // to do 
        * void setFilename(String filename)
        * Board build() // build a board from the filename
    * boolean add(Coord coord, int color) // color is -1 from the given color number (beginning from 0)
    * int[] getDistances()
    * String toString() // String from the distances
        ```java
        Board.BoardBuilder bb = new Board.BoardBuilder(filename);
        bb.build();
        ```
    * int[] validPath(Path) // returns {isSuccess, stopped_length}
    * int colorAt(Coord c) // returns the color at the coord c, returns -1 if empty


