package com.coder.addictive.level1;

import java.util.*;

import com.coder.util.*;
import com.coder.addictive.util.Coord;


public class Main {
    public static void main(String[] args) {
        System.out.println("main for addictive/level1");
        String dirName = "/Users/yoju/Proto/coder/inputs/Addictive/level1";
        solveDir(dirName);


        // Coord.main(null);

        // System.out.println("test for ReadInput");
        // ReadInput.main(null);

    }

    public static void solveDir(String inputDir) {
        List<String> inputFiles = ReadInput.readDir(inputDir, ".in");
        if (inputFiles == null) {
            System.out.println("the given directory is not valid");
        }

        for (String filename : inputFiles) {
            System.out.println(filename);
            System.out.println(solve(filename));
        }
    }

    public static String solve(String inputFileName) {
        int[] inputArray = ReadInput.readLineToIntArray(inputFileName);

        if(!isValid(inputArray)) {
            return null;
        }


        Coord.CoordBuilder cb;
        try {
            cb = new Coord.CoordBuilder(inputArray[0], inputArray[1]);
        } catch (Exception e) {
            return null;
        }


        Coord[] coords = new Coord[inputArray[2]];
        for (int i =0; i<inputArray[2]; i++) {
            coords[i] = cb.build(inputArray[3+i]);
        }
        return HelpPrint.arrayToString(coords);

    }

    private static boolean isValid(int[] inputArray) {
        if (inputArray == null || inputArray.length < 3) {
            return false;
        }
        if (inputArray[2] + 3 != inputArray.length) {
            return false;
        }
        return true;
    }

}
