package com.coder.addictive.level2;

import java.util.*;
import com.coder.addictive.util.*;
import com.coder.util.*;

public class Main {
    public static void main(String[] args) {
        String dirName = "/Users/yoju/Proto/coder/inputs/Addictive/level2";
        List<String> files = ReadInput.readDir(dirName, ".in");

        Board.BoardBuilder bb = new Board.BoardBuilder(null);

        for (String filename : files) {
            System.out.println(filename);
            bb.setFilename(filename);
            System.out.println(bb.build());
        }
    }
}
