package com.coder.addictive.util;

import com.coder.util.*;

public class Board {
    private CoordPair[] pairs;
    private Coord.CoordBuilder cb;

    private Board(int numberPairs) {
        this.pairs = new CoordPair[numberPairs];
        for (int i=0; i<numberPairs; i++) {
            this.pairs[i] = new CoordPair();
        }
    }

    /**
     * methods
     **/
    public int[] validPath(Path path) {
        // returns a pair of int
        // check for invalidity
        // 1. goes out of bounds
        // 2. crosses itself
        // 3. touches point of a different color
        // 4. does not end in the corresponding place (but it should not be starting point)
        return null;
    }


    public int colorAt(Coord coord) {
        for(int i=0; i<pairs.length; i++) {
            if (pairs[i].contains(coord)) {
                return i;
            }
        }
        return -1;
    }

    public boolean add(Coord coord, int color) {
        try {
            return pairs[color].add(coord);
        } catch(Exception e) {
            return false;
        }
    }

    public int[] getDistances() {
        int[] dis = new int[pairs.length];
        for (int i=0; i<pairs.length; i++) {
             dis[i] = pairs[i].getDistance();
        }
        return dis;
    }

    public void setCoordBuilder(Coord.CoordBuilder cb) {
        this.cb = cb;
    }

    @Override
    public String toString() {
        return HelpPrint.arrayToString(getDistances());
    }

    /**
     * builder for board
     **/
    public static class BoardBuilder {
        public String filename;     // for l1, l2
        public BoardBuilder(String filename) {
            this.filename = filename; 
        }
        public void setFilename(String filename) {
            this.filename = filename;
        }
        public Board build(){
            // read the file into int array
            int[] inputArray = ReadInput.readLineToIntArray(this.filename);

            // check the validity of the array
            if (!isValid(inputArray)) {
                System.out.println("the input file is not valid");
                return null;
            }
            
            // create new Board
            int numPoints = inputArray[2];
            Board board = new Board(numPoints/2);
            
            // create a coordbuilder
            Coord.CoordBuilder cb = new Coord.CoordBuilder(inputArray[0], inputArray[1]);

            // add the points
            int index = 2;
            for (int i=0; i<numPoints; i++) {
                int position = inputArray[++index];
                int color = inputArray[++index]; 
                // System.out.println("position " + position + " color " + color);
                board.add(cb.build(position), color - 1);
            }

            return board;
        }

        private boolean isValid(int[] input) {
            if (input == null) {
                System.out.println("file wrong");
                return false;
            }
            // board size check
            if ( input[0] <= 0 || input[1] <= 0) {
                return false;
            }
            // number of points check
            if (input[2] <= 0 || input[2] % 2 != 0) {
                System.out.println(input[2] + " is not proper number of points");
                return false;
            }
            return (input.length) == 2 * input[2] + 3;
        }
        
    }


    /**
     * main method to test the class
     **/
    public static void main(String[] args) {
        String filename = "/Users/yoju/Proto/coder/inputs/sample.in";
        System.out.println("test class Board: open file " + filename);
        BoardBuilder bb = new BoardBuilder(filename);
        Board board = bb.build();

        Coord.CoordBuilder cb = new Coord.CoordBuilder(6,5);
        int p = 15;
        System.out.println("the color at position " + p + " is: " + (board.colorAt(cb.build(p)) + 1));
    }
}
