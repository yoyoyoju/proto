package com.coder.addictive.util;

public class Coord{
    private int[] boardSize;
    private int[] coordPair;


    /**
     * builder for Coord
     **/
    public static class CoordBuilder {
        private int[] boardSize;

        public CoordBuilder(int boardRows, int boardCols) 
                throws IllegalArgumentException {
            if (boardRows <= 0 || boardCols <= 0) {
                throw new IllegalArgumentException("invalid board size");
            }

            this.boardSize = new int[2];
            this.boardSize[0] = boardRows;
            this.boardSize[1] = boardCols;
        }

        public Coord build(int position) 
                throws IllegalArgumentException {
            if (position <= 0 || position > maxPosition()) {
                throw new IllegalArgumentException("invalid position");
            }

            return new Coord(boardSize, getPair(position));
        }

        private int[] getPair(int position) {
            int p0 = position - 1;
            int[] coord = new int[2]; 
            coord[0] = p0 / boardSize[1] + 1;
            coord[1] = p0 % boardSize[1] + 1 ;
            return coord;
        }

        private int maxPosition() {
            return boardSize[0] * boardSize[1];
        }
    }


    /** 
     * constructor
     * private: cannot be called - use builder
     **/

    private Coord(int[] boardSize, int[] coordPair) {
        this.boardSize = boardSize;
        this.coordPair = coordPair;
    }

    /**
     * getters
     **/
    public int[] getBoardSize() {
        return this.boardSize;
    }

    public int[] getCoordPair() {
        return this.coordPair;
    }


    /**
     * methods
     **/

    @Override
    public String toString() {
        return coordPair[0] + " " + coordPair[1];
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Coord compared = (Coord) object;
        if (!Coord.areEqual(boardSize, compared.getBoardSize())) {
            return false;
        }
        if (!Coord.areEqual(coordPair, compared.getCoordPair())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = java.util.Arrays.hashCode(boardSize);
        result = 31 * result + java.util.Arrays.hashCode(coordPair);
        return result;
    }

    private static boolean areEqual(int[] a, int[] b) {
        int len = a.length;
        if (len != b.length) {
            return false;
        }
        for (int i=0;i<len; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    public static int manhattanDistance(Coord c1, Coord c2) {
        return Math.abs(c1.coordPair[0] - c2.coordPair[0]) +
            Math.abs(c1.coordPair[1] - c2.coordPair[1]);
    }

    /**
     *  main function to test the class
     **/
    public static void main(String[] args){ 
        // test
        Coord.CoordBuilder cb = new Coord.CoordBuilder(6, 5);
        Coord coord1 = cb.build(5);
        Coord coord2 = cb.build(26);
        System.out.println(Coord.manhattanDistance(coord1, coord2));
    }
}
