package com.coder.addictive.util;

public class CoordPair {
    Coord[] pair;

    public CoordPair() {
        pair = new Coord[2];
    }

    public boolean add(Coord c) {
        if (pair[0] == null) {
            pair[0] = c;
            return true;
        } 
        if (pair[1] == null) {
            pair[1] = c;
            return true;
        }
        return false;
    }

    public int getDistance() {
        if (pair[0] == null || pair[1] == null) {
            return -1;
        }
        return Coord.manhattanDistance(pair[0], pair[1]);
    }

    public boolean contains(Coord coord) {
        if (coord == null) {
            return false;
        }
        return coord.equals(pair[0]) || coord.equals(pair[1]) ;
    }


    public static void main(String[] args) {
        Coord.CoordBuilder cb = new Coord.CoordBuilder(6, 5);
        Coord c1 = cb.build(1);
        Coord c2 = cb.build(12);
        CoordPair pair = new CoordPair();
        pair.add(c1);
        pair.add(c2);
        System.out.println(pair.getDistance());
        System.out.println(pair.contains(c1));

    }
}
