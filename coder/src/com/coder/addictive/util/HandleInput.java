package com.coder.addictive.util;


import java.util.*;
import com.coder.util.*;

public class HandleInput {
    private String[] inputArray;
    private Path[] paths;
    private int[] boardInput;

    public HandleInput(String[] inputArray) {
        this.inputArray = inputArray;

        // get boardInput
        int staInd = 0;
        int endInd = 2 * Integer.parseInt(inputArray[2]) + 3;
        this.boardInput = ReadInput.toIntArray(Arrays.copyOfRange(inputArray, staInd, endInd));

        // get paths
        paths = new Path[Integer.parseInt(inputArray[endInd])];
        endInd++;

        for(int i=0; i<paths.length; i++) {
            int color = Integer.parseInt(inputArray[endInd]);
            int position = Integer.parseInt(inputArray[++endInd]);
            int length = Integer.parseInt(inputArray[++endInd]);
            staInd = ++endInd;
            endInd += length;
            String[] movement = Arrays.copyOfRange(inputArray, staInd, endInd);

            paths[i] = new Path(color, position, movement);
        }
    }

    public int[] getBoardInput() {
        return boardInput;
    }

    public Path[] getPaths() {
        return paths;
    }

    public static void main(String[] args) {
        System.out.println("main class from HandleInput class");
        String example = "5 5 8 7 1 9 1 10 2 16 3 17 2 19 4 20 3 25 4 1 3 16 8 S E E N N E E S";
        example = "5 5 8 7 1 9 1 10 2 16 3 17 2 19 4 20 3 25 4 2 3 16 8 S E E N N E E S 1 10 3 S E N";
        HandleInput hi = new HandleInput(example.split(" "));
        // System.out.println(HelpPrint.arrayToString(hi.getBoardInput()));
    }
}
