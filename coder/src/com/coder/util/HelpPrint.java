package com.coder.util;

public class HelpPrint {
    public static void main(String[] args) {
        // to test methods
        String[] ex = {"a", "b", "c"};
        System.out.println(arrayToString(ex));
        int[] intex = {1, 2, 3};
        System.out.println(arrayToString(intex, " "));
    }

    public static <T> String arrayToString(T[] inputArray) {
        return arrayToString(inputArray, " ");
    }

    public static <T> String arrayToString(T[] inputArray, String splitSt) {
        String sp = splitSt;
        if (splitSt==null) {
            sp = "";
        }

        StringBuilder sb = new StringBuilder();
        for (T t : inputArray) {
            sb.append(t).append(sp);
        }

        // delete the last splitSt
        if (sp.length() > 0) {
            int l = sb.length();
            sb.delete(l - sp.length(), l);
        }
        return sb.toString();
    }

    public static String arrayToString(int[] inputArray) {
        return arrayToString(inputArray, " ");
    }

    public static String arrayToString(int[] inputArray, String splitSt) {
        String sp = splitSt;
        if (splitSt==null) {
            sp = "";
        }

        StringBuilder sb = new StringBuilder();
        for (int t : inputArray) {
            sb.append(t).append(sp);
        }

        // delete the last splitSt
        if (sp.length() > 0) {
            int l = sb.length();
            sb.delete(l - sp.length(), l);
        }
        return sb.toString();
    }
        
}
