package com.coder.util;

import java.io.File;
import java.util.Scanner;

// for readDir method
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;

// 
import com.coder.util.HelpPrint;

public class ReadInput {
    public static void main(String[] args) {
        // test ReadInput class
        System.out.println(readLine("/Users/yoju/Proto/coder/inputs/sample.txt"));
        System.out.println(HelpPrint.arrayToString(readLineToArray("/Users/yoju/Proto/coder/inputs/sample.txt", " ")));
        System.out.println(HelpPrint.arrayToString(readLineToIntArray("/Users/yoju/Proto/coder/inputs/sample.in")));
        List<String> files = readDir("/Users/yoju/Proto/coder/inputs/Addictive", ".in");
        System.out.println(files);
    }

    public static List<String> readDir(String dirName) {
        try {
        List<String> filesInDir = Files.walk(Paths.get(dirName))
            .filter(Files::isRegularFile)
            .map(Path::toString)
            .collect(Collectors.toList());
        return filesInDir;
        } catch (Exception e) {
            return null;
        }
    }

    public static List<String> readDir(String dirName, String extension) {
        // only files ends with extension
        List<String> fileInDir = readDir(dirName);
        if (fileInDir == null || fileInDir.size() == 0) {
            return null;
        }

        Iterator<String> iterator = fileInDir.iterator();
        while(iterator.hasNext()) {
            if (!iterator.next().toLowerCase().endsWith(extension)) {
                iterator.remove();
            }
        }
        return fileInDir;
    }

    public static String readLine(String filename) {
        // return the first line of the given file name of filename
        // in the case of exception or empty file, it returns null
        File file = new File(filename);
        Scanner reader = null;
        try {
            reader = new Scanner(file);
        } catch (Exception e) {
            return null;
        }
        String result = null;
        if (reader.hasNextLine()) {
            result = reader.nextLine();
        }
        reader.close();
        return result;
    }

    public static String[] readLineToArray(String filename, String splitSt) {
        // return a array of String
        // from the first line of the file with filename
        // split by the splitSt
        // in the case of exception or empty file, it returns null
        String firstLine = readLine(filename);
        if (firstLine == null) {
            return null;
        }

        return firstLine.split(splitSt);
    }

    public static int[] readLineToIntArray(String filename) {
        // do it
        String[] strArr = readLineToArray(filename, " ");

        return toIntArray(strArr);
    }

    public static int[] toIntArray(String[] strArray) {
        if (strArray == null) {
            return null;
        }
        int[] result = new int[strArray.length];
        for (int i=0; i<strArray.length; i++) {
            try {
                result[i] = Integer.parseInt(strArray[i]);
            } catch (Exception e) {
                return null;
            }
        }
        return result;
    }

}
