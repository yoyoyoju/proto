
Resources:
  sslSecurityGroupIngress: 
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: {"Fn::GetAtt" : ["AWSEBSecurityGroup", "GroupId"]}
      IpProtocol: tcp
      ToPort: 443
      FromPort: 443
      CidrIp: 0.0.0.0/0

files:
  # redirect http to docker server.
  /etc/nginx/conf.d/http_custom_proxy.conf:
    mode: "000644"
    owner: root
    group: root
    content: |
      server {
        listen 80;
        return 301 https://$host$request_uri;
      }

  # server configuration for handling TLS termination
  /etc/nginx/conf.d/https_custom.pre:
    mode: "000644"
    owner: root
    group: root
    content: |
      # HTTPS Server

      server {
        listen 443;
        server_name localhost;

        ssl on;

        ssl_certificate      /etc/letsencrypt/live/ebcert/fullchain.pem;
        ssl_certificate_key  /etc/letsencrypt/live/ebcert/privkey.pem;

        ssl_session_timeout 5m;

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
        ssl_prefer_server_ciphers on;

        location / {
          proxy_pass http://docker;
          proxy_http_version 1.1;

          proxy_set_header Connection "";
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
      }


# container configuration
packages:
  yum:
    epel-release: []

container_commands:
  00_create_dir:
    command: "mkdir -p /opt/certbot"
  10_installcertbot:
    command: "wget https://dl.eff.org/certbot-auto  -O /opt/certbot/certbot-auto"
  20_permission: 
    command: "chmod a+x /opt/certbot/certbot-auto"
  30_getcert:
    command: "sudo /opt/certbot/certbot-auto certonly --debug --non-interactive --email yoijuyoju@gmail.com --agree-tos --standalone --domains portfolio.zzzit.net --keep-until-expiring --pre-hook \"service nginx stop\" "
  40_link:
    command: "sudo ln -sf /etc/letsencrypt/live/portfolio.zzzit.net /etc/letsencrypt/live/ebcert"
  50_config:
    command: "sudo mv /etc/nginx/conf.d/https_custom.pre /etc/nginx/conf.d/https_custom.conf"
  60_restartnginx:
    command: "sudo service nginx start"
  70_setrenewal:
    command: "(crontab -l ; echo '0 6 * * * root /opt/certbot/certbot-auto renew --standalone --pre-hook \"service nginx stop\" --post-hook \"service nginx start\" --force-renew') | crontab -"
