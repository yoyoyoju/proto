# setting up a static website
*following the [tutorial](https://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html)

1. Register a Domain

* S3:
    2. create buckets matching the domain
    3. upload the website data
    4. set the public access setting: enable
    5. set the policy
    6. configure Buckets for website hosting
        * static website hosting
    7. configure the redirect

* Route 53: as root
    8. hosted zones -> create Record Set
        * name
        * Type: A-IPv4 address
        * Alias: Yes
        * Alias Target: s3 bucket endpoint
        * Routing Policy: Simple
        * Evaluate Target Health: No
        * Create
